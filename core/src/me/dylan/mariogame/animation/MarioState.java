package me.dylan.mariogame.animation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public enum MarioState {
	
	IDLE_RIGHT(1F, PlayMode.LOOP, "SmallIdleRight0000"), 
	IDLE_LEFT(1F, PlayMode.LOOP, "SmallIdleLeft0000"),
	WALK_RIGHT(1/5F, PlayMode.LOOP, "SmallWalkRight0000", "SmallWalkRight0001", "SmallWalkRight0002"),
	WALK_LEFT(1/5F, PlayMode.LOOP, "SmallWalkLeft0000", "SmallWalkLeft0001", "SmallWalkLeft0002"),
	JUMP_RIGHT(1F, PlayMode.LOOP, "SmallJumpRight0000"),
	JUMP_LEFT(1F, PlayMode.LOOP, "SmallJumpLeft0000");
	
	private final TextureAtlas marioAtlas;
	private final Animation animation;
	
	MarioState(float frameDuration, PlayMode playMode, String... regionNames) {
		marioAtlas = new TextureAtlas(Gdx.files.internal("sprites/mario-spritesheet.atlas"));
		TextureRegion[] textureRegions = new TextureRegion[regionNames.length];
		for (int i = 0; i < regionNames.length; i ++)
			textureRegions[i] = marioAtlas.findRegion(regionNames[i]);
		this.animation = new Animation(frameDuration, textureRegions);
		this.animation.setPlayMode(playMode);
		System.out.println("GI");
	}
	
	public Animation getAnimation() {
		return this.animation;
	}
	
	/* OLD CONSTRUCTOR:
	 MarioState(float frameDuration, boolean looping, Rectangle... regions) {
		marioSpriteSheet = new Texture(Gdx.files.internal("sprites/mario-spritesheet.gif"));
		TextureRegion[] textureRegions = new TextureRegion[regions.length];
		for (int i = 0; i < regions.length; i ++)
			textureRegions[i] = new TextureRegion(marioSpriteSheet, (int) regions[i].getX(), (int) regions[i].getY(),
					(int) regions[i].getWidth(), (int) regions[i].getHeight());
		this.animation = new Animation(frameDuration, textureRegions);
		if (looping) this.animation.setPlayMode(PlayMode.LOOP); //todo change boolean to playmode
	}*/
	
}
