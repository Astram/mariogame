package me.dylan.mariogame.audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

public enum MarioEffect {
	
	JUMP(Gdx.files.internal("audio/sound/jump.wav")),
	PIPE(Gdx.files.internal("audio/sound/pipe.wav")),
	HERE_WE_GO(Gdx.files.internal("audio/sound/herewego.wav")),
	DEATH(Gdx.files.internal("audio/sound/death.wav")),
	WIN(Gdx.files.internal("audio/sound/win.wav"));
	
	private Sound sound;
	
	MarioEffect(FileHandle fileHandle) {
		sound = Gdx.audio.newSound(fileHandle);
	}
	
	public Sound getSound() {
		return sound;
	}
	
}
