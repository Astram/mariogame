package me.dylan.mariogame.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import me.dylan.mariogame.MarioGame;

public class LoginScreen implements Screen {
	
	private MarioGame game;
	
	private Stage stage;
	private Table table;
	
	private Texture background;
	private Skin skin;
	
	private Sound oneUp;
	
	public LoginScreen(MarioGame game) {
		this.game = game;
	}

	@Override
	public void show() {
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		
		/*
		table = new Table();
	    table.setFillParent(true);
	    stage.addActor(table);
	    
	    table.setDebug(false);*/
	    
	    skin = new Skin(Gdx.files.internal("ui/uiskin.json"));
	    
	    //UI Elements
	    
	    Dialog dialog = new Dialog("Login", skin);
	    dialog.setMovable(false);
	    dialog.setSize(300F, 200F);
	    
	    Label userLabel = new Label("Username:", skin);
	    userLabel.setPosition(100, 155);
	    
	    Label passLabel = new Label("Password:", skin);
	    passLabel.setPosition(100, 105);
	    
	    final TextField userName = new TextField("", skin);
	    
	    userName.setSize(150F, 25F);
	    userName.setPosition(75, 130);
	    
	    final TextField password = new TextField("", skin);
	    
	    password.setPasswordMode(true);
	    password.setPasswordCharacter('*');
	    password.setSize(150F, 25F);
	    password.setPosition(75, 80);
	    
	    TextButton signInButton = new TextButton("Sign In", skin, "default");
	    signInButton.setPosition(dialog.getWidth() / 2 - signInButton.getWidth() / 2, 30);
	    
	    dialog.addActor(userName);
	    dialog.addActor(signInButton);
	    dialog.addActor(password);
	    dialog.addActor(userLabel);
	    dialog.addActor(passLabel);
	    
	    dialog.setPosition(Gdx.graphics.getWidth() / 2 - dialog.getWidth() / 2, 0);
	    
	    final TextButton guestButton = new TextButton("Play as Guest", skin);
	    guestButton.setPosition(Gdx.graphics.getWidth() - guestButton.getWidth() - 30, Gdx.graphics.getHeight() - guestButton.getHeight() - 30);
	    
	    guestButton.addListener(new ClickListener(Buttons.LEFT) {
	    	
		    @Override
		    public void clicked(InputEvent event, float x, float y) {
		    	game.setScreen(new MenuScreen(game));
		    }
	    	
	    });
	    
	    MoveToAction moveAction = new MoveToAction();
		moveAction.setInterpolation(Interpolation.pow5);
		moveAction.setDuration(.5F);
		moveAction.setPosition(Gdx.graphics.getWidth() / 2 - dialog.getWidth() / 2, Gdx.graphics.getHeight() / 2 - dialog.getHeight() / 2);
	    
		dialog.addAction(moveAction);
		
	    stage.addActor(dialog);
	    stage.addActor(guestButton);
	    
		background = new Texture(Gdx.files.internal("backgrounds/background.png"));
		
		oneUp = Gdx.audio.newSound(Gdx.files.internal("audio/sound/1up.wav"));
		oneUp.play();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1F, 1F, 1F, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.batch.setColor(Color.WHITE);
		game.batch.begin();
		game.batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		game.batch.end();
		stage.act(delta);
	    stage.draw();
	}

	@Override
	public void dispose() {
		oneUp.dispose();
		background.dispose();
		stage.dispose();
		skin.dispose();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		
	}

}
