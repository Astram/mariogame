package me.dylan.mariogame.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import me.dylan.mariogame.MarioGame;

public class SplashScreen implements Screen {
	
	private final MarioGame game;
	
	private Texture logo;
	
	private int width = 0;
	private int height = 0;
	
	private float renders = 0;
	boolean increasing = true;
	
	public SplashScreen(MarioGame marioGame) {
		this.game = marioGame;
	}

	@Override
	public void show() {
		logo = new Texture(Gdx.files.internal("logos/Super_Mario_Logo.png"));
		width = Gdx.graphics.getWidth();
		height = Gdx.graphics.getHeight();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.batch.begin();
		game.batch.setColor(new Color(255F, 255F, 255F, renders));
		game.batch.draw(logo, width / 2 - logo.getWidth() / 4, height / 2 - logo.getHeight() / 4, logo.getWidth() / 2, logo.getHeight() / 2);
		game.batch.end();
		if (increasing) {
			if (renders < 1) {
				renders += .0075;
			} else {
				increasing = false;
			}
		} else {
			if (renders > 0) {
				renders -= .0075;
			} else { //next state
				game.setScreen(new LoginScreen(game));
			}
		}
		if (Gdx.input.isButtonPressed(Input.Buttons.LEFT))
			game.setScreen(new LoginScreen(game));
	}

	@Override
	public void dispose() {
		logo.dispose();
	}
	
	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		
	}

}
