package me.dylan.mariogame.states;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import me.dylan.mariogame.MarioGame;
import me.dylan.mariogame.animation.MarioState;
import me.dylan.mariogame.audio.MarioEffect;
import me.dylan.mariogame.components.Entity;
import me.dylan.mariogame.components.audio.MusicPlayer;
import me.dylan.mariogame.components.audio.SoundPlayer;
import me.dylan.mariogame.components.logic.MarioMovement;
import me.dylan.mariogame.components.logic.MarioRespawn;
import me.dylan.mariogame.components.physics.Fixture;
import me.dylan.mariogame.components.physics.Rigidbody;
import me.dylan.mariogame.components.physics.Shape;
import me.dylan.mariogame.components.rendering.AnimationRenderer;

public class GameScene implements Screen, InputProcessor {
	
	private OrthographicCamera camera;
	private TiledMap tiledMap;
	private TiledMapRenderer tiledMapRenderer;
	private final MarioGame game;
	private final String mapName;
	
	private World world;
	
	private Texture square;
	
	private float cameraHeight;
	
	private Set<Entity> entities = new HashSet<Entity>();
	private Entity mario;
	private Entity musicPlayer;
	private Entity soundPlayer;
	
	//hud
	private Stage stage;
	private Skin skin;
	private Label livesLabel;
	private int lives;
	private boolean won;
	
	public GameScene(MarioGame game, String mapName) {
		this.game = game;
		this.mapName = mapName;
	}
	
	@Override
	public void show() {
		float width = Gdx.graphics.getWidth();
        float height = Gdx.graphics.getHeight();
        
        stage = new Stage();
		skin = new Skin(Gdx.files.internal("ui/uiskin.json"));
		
		lives = 3;
		livesLabel = new Label("Lives: " + lives, skin);
		livesLabel.setPosition(10, height - livesLabel.getHeight() - 10);
		
		stage.addActor(livesLabel);
        
        camera = new OrthographicCamera();
        camera.setToOrtho(false, width / 2, height / 2F);
        camera.update();
        if (mapName != null)
        	tiledMap = new TmxMapLoader().load("maps/" + mapName + ".tmx");
        else
        	tiledMap = new TmxMapLoader().load("maps/testmap.tmx");
        tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);
        RectangleMapObject start = (RectangleMapObject) tiledMap.getLayers().get("LevelData").getObjects().get("start");
        
        //box2d physics
        world = new World(new Vector2(0, -9.8F * 16F), true);
        
        MusicPlayer mpComp = new MusicPlayer("audio/music/overworld.mp3");
        mpComp.getMusic().setLooping(true);
        mpComp.getMusic().play();
        musicPlayer = new Entity(mpComp);
        
        //mario entity
        
        AnimationRenderer arComp = new AnimationRenderer(game.batch, MarioState.IDLE_RIGHT.getAnimation());
        Rigidbody rbComp = new Rigidbody(start.getRectangle().getX(), start.getRectangle().getY());
        rbComp.setWorld(world);
        rbComp.setBodyType(BodyType.DynamicBody);
        Shape sComp = new Shape(12, 16);
        Fixture fComp = new Fixture(5F);  
        SoundPlayer spmComp = new SoundPlayer();
        MarioMovement mmComp = new MarioMovement();
        MarioRespawn mrComp = new MarioRespawn(this, camera, mpComp, new Vector2(start.getRectangle().x, start.getRectangle().y), Integer.parseInt((String) tiledMap.getLayers().get("LevelData").getProperties().get("DeathHeight")));
        mario = new Entity(rbComp, arComp, sComp, fComp, spmComp, mmComp, mrComp);
        won = false;
        
        SoundPlayer spComp = new SoundPlayer();
        soundPlayer = new Entity(spComp);
        
        entities.addAll(Arrays.asList(mario, musicPlayer, soundPlayer));
        ((SoundPlayer) soundPlayer.getComponent(SoundPlayer.class)).playSound(MarioEffect.HERE_WE_GO.getSound());
        
        //tile collisions

        TiledMapTileLayer collisions = (TiledMapTileLayer) tiledMap.getLayers().get("Foreground");
        //MathUtils.getVertices(collisions);
        for (int x = 0; x < collisions.getWidth(); x ++) {
        	for (int y = 0; y < collisions.getHeight(); y ++) {
        		TiledMapTileLayer.Cell cell = collisions.getCell(x, y);
        		if (cell == null) continue;
        		if (cell.getTile() == null) continue;
        		BodyDef colBodyDef = new BodyDef();
        		colBodyDef.position.set(collisions.getTileWidth() * x, collisions.getTileHeight() * y);
        		colBodyDef.type = BodyDef.BodyType.StaticBody;
        		Body colBody = world.createBody(colBodyDef);
        		
        		PolygonShape colShape = new PolygonShape();
        		colShape.setAsBox(collisions.getTileWidth() / 2, collisions.getTileHeight() / 2,
        				new Vector2(collisions.getTileWidth() / 2 - 8, collisions.getTileHeight() / 2 - 8), 0);
        		FixtureDef colFixture = new FixtureDef();
        		colFixture.density = 0;
        		colFixture.friction = .9999999F;
        		colFixture.shape = colShape;
        		colBody.createFixture(colFixture);
        	}
        }
        
        
        /*for (RectangleMapObject mapObject : tiledMap.getLayers().get("Collisions").getObjects().getByType(RectangleMapObject.class)) {
        	if (mapObject.getName() == null || !mapObject.getName().equals("start")) {
        		rectangle = mapObject.getRectangle();
        		BodyDef colBodyDef = new BodyDef();
        		colBodyDef.position.set(rectangle.getX(), rectangle.getY());
        		colBodyDef.type = BodyDef.BodyType.StaticBody;
        		Body colBody = world.createBody(colBodyDef);
        		
        		PolygonShape colShape = new PolygonShape();
        		colShape.setAsBox(rectangle.getWidth() / 2, rectangle.getHeight() / 2, 
        				new Vector2(rectangle.getWidth() / 2 - 8, rectangle.getHeight() / 2 - 8), 0);
        		FixtureDef colFixture = new FixtureDef();
        		colFixture.density = 0;
        		colFixture.friction = .9999999F;
        		colFixture.shape = colShape;
        		colBody.createFixture(colFixture);
        	}
        }*/
        
        Gdx.input.setInputProcessor(this);
        cameraHeight = camera.position.y + 16 * 4;
        camera.position.set(camera.position.x, cameraHeight, 0);
        square = new Texture(Gdx.files.internal("sprites/pixel.gif"));
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		world.step(delta, 6, 2);
		tiledMapRenderer.render();
        camera.update();
        tiledMapRenderer.setView(camera);
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.batch.setColor(new Color(1, 1, 1, 1));
        game.font.setColor(Color.WHITE);
		//update entities
		updateEntities();
		//mario.setPosition(mario.getPhysicsBody().getPosition().x, mario.getPhysicsBody().getPosition().y);
		if (((AnimationRenderer) mario.getComponent(AnimationRenderer.class)).getX() - camera.viewportWidth / 2 > 0)
			camera.position.set(new Vector2(((AnimationRenderer) mario.getComponent(AnimationRenderer.class)).getX(), cameraHeight), 0);
        
        game.batch.setColor(new Color(1, 0, 0, .3F));
	    /*for (RectangleMapObject object : tiledMap.getLayers().get("Collisions").getObjects().getByType(RectangleMapObject.class)) {
	    	if (object.getName() == null || !object.getName().equalsIgnoreCase("start"))
	    		game.batch.draw(square, (int) object.getRectangle().getX(), (int) object.getRectangle().getY(), 0, 0, (int) object.getRectangle().getWidth(), (int) object.getRectangle().getHeight());
        }*/
        game.batch.end();
        //gui
        stage.act(delta);
		stage.draw();
		//check win condition
		if (!won)
			checkFinish();
	}
	
	private void updateEntities() {
		for (Entity entity : entities)
			entity.update();
	}
	
	private void checkFinish() {
		Vector2 currentPosition = ((Rigidbody) mario.getComponent(Rigidbody.class)).getBody().getPosition();
		//int cellx = (int) (currentPosition.x / 16);
		Vector2 tilePosition = new Vector2((int) (currentPosition.x / 16) + 1, (int) (currentPosition.y / 16));
		Cell cell = ((TiledMapTileLayer) tiledMap.getLayers().get("Foreground")).getCell((int) tilePosition.x, (int) tilePosition.y);
		if (cell != null && cell.getTile() != null) {
			if (cell.getTile().getId() == 103) { //touches victory pole
				won = true;
				System.out.println("You win!");
				Rigidbody rigidbody = (Rigidbody) mario.getComponent(Rigidbody.class);
				rigidbody.getBody().setTransform(currentPosition.add(16, 0), 0);
				((SoundPlayer) soundPlayer.getComponent(SoundPlayer.class)).playSound(MarioEffect.WIN.getSound());
				final Vector2 newPosition = rigidbody.getBody().getPosition();
				Vector2 target = getPoleEnd(tilePosition);
				Timer timer = new Timer();
				mario.removeComponent(mario.getComponent(MarioMovement.class));
				((MusicPlayer) musicPlayer.getComponent(MusicPlayer.class)).getMusic().stop();
				timer.scheduleTask(new Task() {
					
					float time;
					
					@Override
					public void run() {
						time += .02F;
						rigidbody.getBody().setTransform(newPosition.interpolate(target, (float) time / 5F, Interpolation.linear), 0);
						if (time >= 5) {
							dispose();
							game.setScreen(new MenuScreen(game));
						}
					}
					
				}, 0F, .02F);
			}
		}
	}
	
	private Vector2 getPoleEnd(Vector2 tile) {
		TiledMapTileLayer layer = (TiledMapTileLayer) tiledMap.getLayers().get("Foreground");
		int length = 1;
		while (true) {
			Cell cell = layer.getCell((int) tile.x, (int) tile.y - length);
			if (cell != null && cell.getTile() != null) {
				if (cell.getTile().getId() == 103)
					length++;
				else
					return new Vector2(tile.x * 16, tile.y * 16 - length * 16 + 16);
			}
		}
	}
	
	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void dispose() {
		stage.dispose();
		tiledMap.dispose();
		skin.dispose();
		((MusicPlayer) musicPlayer.getComponent(MusicPlayer.class)).getMusic().stop();
		((MusicPlayer) musicPlayer.getComponent(MusicPlayer.class)).getMusic().dispose();
		square.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
    public boolean keyUp(int keycode) {
        return false;
    }

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (button == Input.Buttons.LEFT) {
			Vector3 loc = camera.unproject(new Vector3(screenX, screenY, 0));
			((Rigidbody) mario.getComponent(Rigidbody.class)).getBody().setTransform(new Vector2(loc.x, loc.y), 0);
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
	
	public void setLives(int lives) {
		if (lives <= 0) {
			dispose();
			game.setScreen(new MenuScreen(game));
		}
		this.lives = lives;
		this.livesLabel.setText("Lives: " + lives);
	}
	
	public int getLives() {
		return lives;
	}
	
}
