package me.dylan.mariogame.states;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import me.dylan.mariogame.MarioGame;

public class MenuScreen implements Screen {
	
	private MarioGame game;
	private Stage stage;
	private Skin skin;
	private Texture background;
	
	public MenuScreen(MarioGame game) {
		this.game = game;
	}

	@Override
	public void show() {
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		skin = new Skin(Gdx.files.internal("ui/uiskin.json"));
		
		SelectBox<String> mapSelector = new SelectBox<String>(skin);
		mapSelector.setSize(300, 20);
		mapSelector.setPosition(Gdx.graphics.getWidth() / 2 - mapSelector.getWidth() / 2, 
				Gdx.graphics.getHeight() / 2 - mapSelector.getHeight() / 2);
		FileHandle fileHandle = Gdx.files.internal("./bin/maps");
		List<String> litems = new ArrayList<String>();
		for (FileHandle file : fileHandle.list()) {
			if (!file.nameWithoutExtension().trim().equals(""))
				litems.add(file.nameWithoutExtension());
		}
		String[] items = new String[litems.size()];
		for (int i = 0; i < items.length; i ++)
			items[i] = litems.get(i);
		mapSelector.setItems(items);
		
		TextButton playButton = new TextButton("Play", skin);
		playButton.setSize(60, 35);
		playButton.setPosition(Gdx.graphics.getWidth() / 2 - playButton.getWidth() / 2,
				Gdx.graphics.getHeight() / 2 - playButton.getHeight() * 2);
		
		playButton.addListener(new ClickListener(Buttons.LEFT) {
			
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new GameScene(game, mapSelector.getSelected()));
			}
			
		});
		
		stage.addActor(mapSelector);
		stage.addActor(playButton);
		
		background = new Texture(Gdx.files.internal("backgrounds/menubg.png"));
	}

	@Override
	public void render(float delta) {
		Gdx.gl20.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.batch.begin();
		game.batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		game.batch.end();
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
		skin.dispose();
		background.dispose();
	}

}
