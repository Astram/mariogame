package me.dylan.mariogame.account;

import java.io.UnsupportedEncodingException;

import me.dylan.mariogame.MarioGame;

public class AccountManager {
	
	private final MarioGame game;
	
	public AccountManager(MarioGame game) {
		this.game = game;
	}

	public void createAccount(String username, String password, String email) {
		byte[] salt = Passwords.getNextSalt();
		byte[] hashedPassword = Passwords.hash(password.toCharArray(), salt);
		String hashedPasswordString = "";
		String saltString = "";
		try {
			hashedPasswordString = new String(hashedPassword, "ISO-8859-1");
			saltString = new String(salt, "ISO-8859-1");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
	}
	
}
