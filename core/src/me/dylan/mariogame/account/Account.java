package me.dylan.mariogame.account;

public class Account {
	
	public String userName;
	public String email;
	
	public Account(String userName, String email) {
		this.userName = userName;
		this.email = email;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public String getEmail() {
		return email;
	}
	
}
