package me.dylan.mariogame.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;

import me.dylan.mariogame.animation.MarioState;
import me.dylan.mariogame.audio.MarioEffect;

public class Mario extends Sprite {
	
	private boolean isBig = false;
	private MarioState animationState;
	private float animationTime = 0;
	private Body physicsBody;
	
	public Mario() {
		animationState = MarioState.IDLE_RIGHT;
	}
	
	@Override
	public void draw(Batch batch) {
		animationTime += Gdx.graphics.getDeltaTime();
        batch.draw(animationState.getAnimation().getKeyFrame(animationTime, true), getX(), getY());
	}
	
	public void updateAnimationState(MarioState animationState) {
		if (this.animationState != animationState) {
			this.animationState = animationState;
			animationTime = 0;
		}
	}
	
	public MarioState getAnimationState() {
		return animationState;
	}
	
	public void setPhysicsBody(Body body) {
		this.physicsBody = body;
	}
	
	public Body getPhysicsBody() {
		return physicsBody;
	}
	
	public void playSound(MarioEffect sound) {
		sound.getSound().play();
	}
	
}
