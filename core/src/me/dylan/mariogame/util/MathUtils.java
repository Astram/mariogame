package me.dylan.mariogame.util;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;

public class MathUtils {
	
	private static boolean[][] locations;
	
	public static List<Vector2> getVertices(TiledMapTileLayer mapLayer) {
		List<Vector2> vertices = new ArrayList<Vector2>();
		locations = new boolean[mapLayer.getWidth()][mapLayer.getHeight()];
		for (int x = 0; x < mapLayer.getWidth(); x ++) {
			for (int y = 0; y < mapLayer.getHeight(); y++) {
				Cell cell = mapLayer.getCell(x, y);
				if (cell != null && cell.getTile() != null) { //found tile
					if (!locations[x][y]) {
						locations[x][y] = true;
						System.out.println("===============================");
						List<Vector2> points = flood(mapLayer, x, y); //points of each rectangle
						for (Vector2 point : points) {
							System.out.println("X: " + point.x + " || Y: " + point.y);
						}
					}
				} else { //empty space
					locations[x][y] = false;
				}
			}
		}
		return vertices;
	}
	
	/*
	 * TODO
	 * Write recursive method to get all tiles adjacent to it!
	 */
	
	private static List<Vector2> flood(TiledMapTileLayer layer, int x, int y) {
		List<Vector2> tiles = new ArrayList<Vector2>();
		Cell cell = layer.getCell(x, y);
		if (cell == null || cell.getTile() == null)
			return tiles;
		locations[x][y] = true;
		tiles.add(new Vector2(x, y));
		if (x - 1 >= 0)
			tiles.addAll(flood(layer, x - 1, y));
		if (x + 1 < layer.getWidth())
			tiles.addAll(flood(layer, x + 1, y));
		if (y - 1 >= 0)
			tiles.addAll(flood(layer, x, y - 1));
		if (y + 1 < layer.getHeight())
			tiles.addAll(flood(layer, x, y + 1));
		return tiles;
	}
	
}
