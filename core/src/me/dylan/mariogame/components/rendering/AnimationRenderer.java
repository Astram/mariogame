package me.dylan.mariogame.components.rendering;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;

import me.dylan.mariogame.components.Component;
import me.dylan.mariogame.components.Entity;
import me.dylan.mariogame.components.physics.Rigidbody;

public class AnimationRenderer implements Component {
	
	private float x;
	private float y;
	private Entity entity;
	private Animation animation;
	private final Batch batch;
	private float animationTime = 0;
	private boolean gaveCoordinates = false;
	
	public AnimationRenderer(Batch batch, Animation animation) {
		this.batch = batch;
		this.animation = animation;
	}
	
	public AnimationRenderer(Batch batch, Animation animation, float x, float y) {
		this.x = x;
		this.y = y;
		this.batch = batch;
		this.animation = animation;
		gaveCoordinates = true;
	}
	
	public void updateAnimationState(Animation animation) {
		if (this.animation != animation) {
			this.animation = animation;
			animationTime = 0;
		}
	}
	
	private void draw() {
		if (batch.isDrawing())
			batch.draw(animation.getKeyFrame(animationTime, true), x, y);
		else {
			batch.begin();
			draw();
			batch.end();
		}
	}
	
	public Animation getAnimation() {
		return animation;
	}
	
	@Override
	public void add(Entity entity) {
		this.entity = entity;
	}
	
	@Override
	public void update() {
		if (!gaveCoordinates) {
			if (entity.hasComponent(Rigidbody.class)) {
				Rigidbody rigidbody = (Rigidbody) entity.getComponent(Rigidbody.class);
				x = rigidbody.getBody().getPosition().x;
				y = rigidbody.getBody().getPosition().y;
			}
		}
		animationTime += Gdx.graphics.getDeltaTime();
		draw();
	}
	
	@Override
	public void rely() {
		// TODO Auto-generated method stub
		
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
}
