package me.dylan.mariogame.components;

public interface Component {
	
	public void add(Entity entity);
	
	public void rely();
	
	public void update();
	
}
