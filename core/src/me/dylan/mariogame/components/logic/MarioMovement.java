package me.dylan.mariogame.components.logic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;

import me.dylan.mariogame.animation.MarioState;
import me.dylan.mariogame.audio.MarioEffect;
import me.dylan.mariogame.components.Component;
import me.dylan.mariogame.components.Entity;
import me.dylan.mariogame.components.audio.SoundPlayer;
import me.dylan.mariogame.components.physics.Rigidbody;
import me.dylan.mariogame.components.rendering.AnimationRenderer;

/*
 * Entity must contain a Rigidbody and an AnimationRenderer
 */

public class MarioMovement implements Component {
	
	private Entity entity;
	private Rigidbody rigidBody;
	private AnimationRenderer animationRenderer;
	private SoundPlayer soundPlayer;
	private boolean isWalkingHorizontally;
	
	@Override
	public void add(Entity entity) {
		this.entity = entity;
	}

	@Override
	public void rely() {
		if (!(entity.hasComponent(Rigidbody.class) && entity.hasComponent(AnimationRenderer.class) &&
				entity.hasComponent(SoundPlayer.class)))
			entity.removeComponent(this);
		else {
			rigidBody = (Rigidbody) entity.getComponent(Rigidbody.class);
			animationRenderer = (AnimationRenderer) entity.getComponent(AnimationRenderer.class);
			soundPlayer = (SoundPlayer) entity.getComponent(SoundPlayer.class);
		}
	}

	@Override
	public void update() {
		isWalkingHorizontally = false;
		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
			isWalkingHorizontally = true;
			if (!isFalling()) {
				animationRenderer.updateAnimationState(MarioState.WALK_LEFT.getAnimation());
				rigidBody.getBody().applyForceToCenter(new Vector2(256 * rigidBody.getBody().getMass() * -5, 0), true);
			} else {
				animationRenderer.updateAnimationState(MarioState.JUMP_LEFT.getAnimation());
				if (rigidBody.getBody().getLinearVelocity().y < 0)
					rigidBody.getBody().setLinearVelocity(16 * -256, rigidBody.getBody().getLinearVelocity().y - 9.8F * Gdx.graphics.getDeltaTime() * 16 * 16 * 16 * 2);
				//else
					//rigidBody.getBody().setLinearVelocity(16 * -256, rigidBody.getBody().getLinearVelocity().y - 9.8F * Gdx.graphics.getDeltaTime());
			}
			//}
		}
		if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
			isWalkingHorizontally = true;
			if (!isFalling()) {
				animationRenderer.updateAnimationState(MarioState.WALK_RIGHT.getAnimation());
				rigidBody.getBody().applyForceToCenter(new Vector2(256 * rigidBody.getBody().getMass() * 5, 0), true);
			} else {
				animationRenderer.updateAnimationState(MarioState.JUMP_RIGHT.getAnimation());
				if (rigidBody.getBody().getLinearVelocity().y < 0)
					rigidBody.getBody().setLinearVelocity(16 * 256, rigidBody.getBody().getLinearVelocity().y - 9.8F * Gdx.graphics.getDeltaTime() * 16 * 16 * 16 * 2);
			}
		}
		if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
			if (!isFalling()) {
				soundPlayer.playSound(MarioEffect.JUMP.getSound());
				if (animationRenderer.getAnimation().equals(MarioState.WALK_RIGHT.getAnimation()) || animationRenderer.getAnimation().equals(MarioState.IDLE_RIGHT.getAnimation()))
					animationRenderer.updateAnimationState(MarioState.JUMP_RIGHT.getAnimation());
				if (animationRenderer.getAnimation().equals(MarioState.WALK_LEFT.getAnimation()) || animationRenderer.getAnimation().equals(MarioState.IDLE_LEFT.getAnimation()))
					animationRenderer.updateAnimationState(MarioState.JUMP_LEFT.getAnimation());
				if (isWalkingHorizontally) {
					rigidBody.getBody().applyLinearImpulse(rigidBody.getBody().getLinearVelocity().x * 16 * rigidBody.getBody().getMass() * 20, 9.8F * rigidBody.getBody().getMass() * 256 * 25, rigidBody.getBody().getPosition().x, rigidBody.getBody().getPosition().y, true);
				} else {
					rigidBody.getBody().applyLinearImpulse(0, 9.8F * rigidBody.getBody().getMass() * 256 * 20, rigidBody.getBody().getPosition().x, rigidBody.getBody().getPosition().y, true);
				}
			}
		}
		Vector2 linVel = rigidBody.getBody().getLinearVelocity();
		if (linVel.x >= -.05F && .05F >= linVel.x) {
			if (animationRenderer.getAnimation().equals(MarioState.WALK_LEFT.getAnimation()) || animationRenderer.getAnimation().equals(MarioState.JUMP_LEFT.getAnimation())) {
				animationRenderer.updateAnimationState(MarioState.IDLE_LEFT.getAnimation());
			} else if (animationRenderer.getAnimation().equals(MarioState.WALK_RIGHT.getAnimation()) || animationRenderer.getAnimation().equals(MarioState.JUMP_RIGHT.getAnimation())) {
				animationRenderer.updateAnimationState(MarioState.IDLE_RIGHT.getAnimation());
			}
		}
		if (!isFalling() && !isWalkingHorizontally && (linVel.x > .05F || linVel.x < -.05F)) {
			rigidBody.getBody().setLinearVelocity(new Vector2(linVel.x / 1.05F, 0));
		}
	}
	
	private boolean isFalling() {
		return !(rigidBody.getBody().getLinearVelocity().y < .05F &&
				rigidBody.getBody().getLinearVelocity().y > -.05F);
	}
	
}
