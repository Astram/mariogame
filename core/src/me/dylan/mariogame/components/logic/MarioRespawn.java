package me.dylan.mariogame.components.logic;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import me.dylan.mariogame.audio.MarioEffect;
import me.dylan.mariogame.components.Component;
import me.dylan.mariogame.components.Entity;
import me.dylan.mariogame.components.audio.MusicPlayer;
import me.dylan.mariogame.components.audio.SoundPlayer;
import me.dylan.mariogame.components.physics.Rigidbody;
import me.dylan.mariogame.components.rendering.AnimationRenderer;
import me.dylan.mariogame.states.GameScene;

public class MarioRespawn implements Component {
	
	private Entity entity;
	private Rigidbody rigidBody;
	private AnimationRenderer animationRenderer;
	private SoundPlayer soundPlayer;
	private MarioMovement marioMovement;
	
	private GameScene gameScene;
	private OrthographicCamera camera;
	private MusicPlayer musicPlayer;
	private Vector2 spawn;
	private int deathHeight;
	
	public MarioRespawn(GameScene gameScene, OrthographicCamera camera, MusicPlayer musicPlayer, Vector2 spawn, int deathHeight) {
		this.gameScene = gameScene;
		this.musicPlayer = musicPlayer;
		this.camera = camera;
		this.spawn = spawn;
		this.deathHeight = deathHeight;
	}
	
	@Override
	public void add(Entity entity) {
		this.entity = entity;
	}
	
	@Override
	public void rely() {
		if (!(entity.hasComponent(Rigidbody.class) && entity.hasComponent(AnimationRenderer.class) &&
				entity.hasComponent(SoundPlayer.class)) && entity.hasComponent(MarioMovement.class))
			entity.removeComponent(this);
		else {
			rigidBody = (Rigidbody) entity.getComponent(Rigidbody.class);
			animationRenderer = (AnimationRenderer) entity.getComponent(AnimationRenderer.class);
			soundPlayer = (SoundPlayer) entity.getComponent(SoundPlayer.class);
			marioMovement = (MarioMovement) entity.getComponent(MarioMovement.class);
		}
	}

	@Override
	public void update() {
		if (rigidBody.getBody().getPosition().y <= deathHeight) {
			rigidBody.getBody().setTransform(spawn, 0);
			musicPlayer.getMusic().pause();
			soundPlayer.playSound(MarioEffect.DEATH.getSound(), 2);
			gameScene.setLives(gameScene.getLives() - 1);
			Vector2 currentCameraPosition = new Vector2(camera.position.x, camera.position.y);
			Vector2 target = new Vector2(camera.viewportWidth / 2, camera.position.y);
			
			Timer timer = new Timer();
			timer.scheduleTask(new Task() {
				
				float time = 0;
				
				@Override
				public void run() {
					time += .02F;
					Vector2 newPos = currentCameraPosition.interpolate(target, time / 3, Interpolation.linear);
					camera.position.set(newPos, 0);
					if (time >= 3) {
						this.cancel();
					}
				}
				
			}, 0, .02F);
			
			timer.scheduleTask(new Task() {

				@Override
				public void run() {
					musicPlayer.getMusic().play();
				}
				
			}, 3);
		}
	}

}
