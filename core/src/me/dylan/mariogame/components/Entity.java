package me.dylan.mariogame.components;

import java.util.HashSet;
import java.util.Set;

public class Entity {
	
	private Set<Component> entityComponents = new HashSet<Component>();
	
	public Entity() {
		//do nothing
	}
	
	public Entity(Component... components) {
		for (Component comp : components)
			entityComponents.add(comp);
		for (Component comp : components)
			comp.add(this);
		for (Component comp : components)
			comp.rely();
	}
	
	public void addComponent(Component component) {
		entityComponents.add(component);
		component.add(this);
		component.rely();
	}
	
	public void addAll(Component... components) {
		for (Component comp : components)
			entityComponents.add(comp);
		for (Component comp : components)
			comp.add(this);
		for (Component comp : components)
			comp.rely();
	}
	
	public void removeComponent(Component component) {
		entityComponents.remove(component);
	}
	
	public Component getComponent(Class<?> component) {
		for (Component comp : entityComponents) {
			if (component.isInstance(comp))
				return comp;
		}
		return null;
	}
	
	public boolean hasComponent(Class<?> component) {
		for (Component comp : entityComponents) {
			if (component.isInstance(comp))
				return true;
		}
		return false;
	}
	
	public void update() {
		for (Component comp : entityComponents) {
			comp.update();
		}
	}
	
}
