package me.dylan.mariogame.components.physics;

import com.badlogic.gdx.physics.box2d.FixtureDef;

import me.dylan.mariogame.components.Component;
import me.dylan.mariogame.components.Entity;

public class Fixture implements Component {
	
	private Entity entity;
	private FixtureDef fixtureDef;
	private com.badlogic.gdx.physics.box2d.Fixture fixture;
	
	public Fixture(float density) {
		fixtureDef = new FixtureDef();
		fixtureDef.density = density;
	}
	
	@Override
	public void add(Entity entity) {
		this.entity = entity;
	}
	
	public void rely() {
		if (entity.hasComponent(Shape.class) && entity.hasComponent(Rigidbody.class)) {
			fixtureDef.shape = ((Shape) entity.getComponent(Shape.class)).getShape();
			fixture = ((Rigidbody) entity.getComponent(Rigidbody.class)).getBody().createFixture(fixtureDef);
		} else {
			entity.removeComponent(this);
		}
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}
	
}
