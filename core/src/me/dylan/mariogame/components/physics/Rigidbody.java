package me.dylan.mariogame.components.physics;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

import me.dylan.mariogame.components.Component;
import me.dylan.mariogame.components.Entity;

public class Rigidbody implements Component {
	
	private float x;
	private float y;
	private World world;
	
	private Body body;
	private BodyType bodyType;
	
	public Rigidbody(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public void setWorld(World world) {
		this.world = world;
	}
	
	public void setBodyType(BodyType bodyType) {
		this.bodyType = bodyType;
	}
	
	@Override
	public void add(Entity entity) {
		BodyDef bodyDef = new BodyDef();
        bodyDef.type = bodyType;
        bodyDef.position.set(x, y);
        body = world.createBody(bodyDef);
	}
	
	public Body getBody() {
		return body;
	}
	
	@Override
	public void update() {
		
	}

	@Override
	public void rely() {
		
	}

}
