package me.dylan.mariogame.components.physics;

import com.badlogic.gdx.physics.box2d.PolygonShape;

import me.dylan.mariogame.components.Component;
import me.dylan.mariogame.components.Entity;

public class Shape implements Component {
	
	private PolygonShape shape;
	private Entity entity;
	
	public Shape(float width, float height) {
		shape = new PolygonShape();
		shape.setAsBox(width / 2, height / 2);
	}
	
	@Override
	public void add(Entity entity) {
		this.entity = entity;
	}
	
	public PolygonShape getShape() {
		return shape;
	}
	
	@Override
	public void update() {
		
	}

	@Override
	public void rely() {
		// TODO Auto-generated method stub
		
	}
	
}
