package me.dylan.mariogame.components.audio;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;

import me.dylan.mariogame.components.Component;
import me.dylan.mariogame.components.Entity;

public class MusicPlayer implements Component {
	
	private Entity entity;
	private Music music;
	
	public MusicPlayer(String path) {
		music = Gdx.audio.newMusic(Gdx.files.internal(path));
	}
	
	public void setMusic(String path) {
		music = Gdx.audio.newMusic(Gdx.files.internal(path));
	}
	
	public Music getMusic() {
		return music;
	}
	
	@Override
	public void add(Entity entity) {
		this.entity = entity;
	}

	@Override
	public void rely() {
		
	}

	@Override
	public void update() {
		
	}
	
	
	
}
