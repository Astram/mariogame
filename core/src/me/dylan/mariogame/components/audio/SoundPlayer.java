package me.dylan.mariogame.components.audio;

import com.badlogic.gdx.audio.Sound;

import me.dylan.mariogame.components.Component;
import me.dylan.mariogame.components.Entity;

public class SoundPlayer implements Component {
	
	private Entity entity;
	
	public void playSound(Sound sound) {
		sound.play();
	}
	
	public void playSound(Sound sound, float volume) {
		sound.play(volume);
	}
	
	@Override
	public void add(Entity entity) {
		this.entity = entity;
	}

	@Override
	public void rely() {
		
	}

	@Override
	public void update() {
		
	}
	
	
	
}
