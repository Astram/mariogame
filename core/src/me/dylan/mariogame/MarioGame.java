package me.dylan.mariogame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import me.dylan.mariogame.states.SplashScreen;

public class MarioGame extends Game {
	
	public SpriteBatch batch;
	public BitmapFont font;
	
	//private AccountManager accountManager;
	//private MySQL sql;
	
	@Override
	public void create() {
		batch = new SpriteBatch();
		font = new BitmapFont();
		//loadSQL();
		//accountManager = new AccountManager(this);
		this.setScreen(new SplashScreen(this));
	}

	@Override
	public void render() {
		super.render();
	}
	
	public void dispose() {
		batch.dispose();
		font.dispose();
	}
	
}
